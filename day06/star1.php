<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$orbits = explode("\n", $input);
$data = [];
$allNodes = [];
foreach ($orbits as $orbit) {
    list($orbited, $orbiter) = explode(')', $orbit);
    if (!isset($data[$orbited])) {
        $data[$orbited] = [];
    }
    $data[$orbited][] = $orbiter;
    $allNodes[] = $orbiter;
    $allNodes[] = $orbited;
}

$allNodes = array_unique($allNodes);

$result = 0;
foreach ($allNodes as $node) {
    $result += getDepth($data, $node);
}

var_dump($result);

// A - B - C
// [A => B, B => C]
function getDepth($nodes, $target) {
    // echo 'Examining ' . $target . '...' . PHP_EOL;
    $nextNode = null;
    foreach ($nodes as $thing => $node) {
        if (in_array($target, $node)) {
            $nextNode = $thing;
            break;
        }
    }

    if (!$nextNode) {
        return 0;
    }

    return 1 + getDepth($nodes, $nextNode);
}