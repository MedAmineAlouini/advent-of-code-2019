<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$orbits = explode("\n", $input);
$data = [];
$allNodes = [];
foreach ($orbits as $orbit) {
    list($orbited, $orbiter) = explode(')', $orbit);
    if (!isset($data[$orbited])) {
        $data[$orbited] = [];
    }
    $data[$orbited][] = $orbiter;
}

$treeYou = getTree($data, 'YOU');
$treeSan = getTree($data, 'SAN');
$treeCommon = array_intersect($treeYou, $treeSan);
$earliest = array_shift($treeCommon);

echo array_search($earliest, $treeYou) + array_search($earliest, $treeSan);

function getTree($nodes, $target) {
    $result = [];
    do {
        $continue = false;
        foreach ($nodes as $orbited => $orbiters) {
            if (in_array($target, $orbiters)) {
                $target = $orbited;
                $result[] = $orbited;
                $continue = true;
                break;
            }
        }
    } while ($continue);

    return $result;
}

