<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$codes = explode(',', $input);

// Custom
$input = 1;
$output = null;

function getParameterValue($mode, $codes, $place) {
    if ($mode == 1) {
        // Value
        return $codes[$place];
    }
    return $codes[$codes[$place]];
}

function getMode($parameter, $opcode) {
    $opcode = $opcode / 100;
    if ($parameter == 1 && $opcode % 10 > 0) {
        return 1;
    } else if ($parameter == 2 && ($opcode / 10) % 10 > 0) {
        return 1;
    }
    return 0;
}

for ($i = 0, $c = count($codes); $i < $c; $i+=0) {
    $opcode = $codes[$i] % 100;
    switch ($opcode) {
        case '1':
            $result = getParameterValue(getMode(1, $codes[$i]), $codes, $i + 1)
                    + getParameterValue(getMode(2, $codes[$i]), $codes, $i + 2);

            $codes[$codes[$i + 3]] = $result;
            $i += 4;
            break;
        case '2':
            $result = getParameterValue(getMode(1, $codes[$i]), $codes, $i + 1)
                    * getParameterValue(getMode(2, $codes[$i]), $codes, $i + 2);

            $codes[$codes[$i + 3]] = $result;
            $i += 4;
            break;
        case '3':
            if ($input !== null) {
                $codes[$codes[$i + 1]] = $input;
            }
            $i += 2;
            break;
        case '4':
            $output = $codes[$codes[$i + 1]];
            $i += 2;
            break;
        case '99':
            break 2;
        default:
            echo 'wat. ' . $opcode . PHP_EOL;
            exit(1);
    }
}

echo $output;