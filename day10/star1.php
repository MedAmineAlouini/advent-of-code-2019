<?php

$input = file_get_contents('input.txt');
$rows = explode("\n", $input);
$map = [];
$potentials = [];

for ($y = 0, $countY = count($rows); $y < $countY; $y++) {
    $columns = str_split($rows[$y]);
    for ($x = 0, $countX = count($columns); $x < $countX; $x++) {
        $map[$y][$x] = $columns[$x] === '#' ? true : false;
        if ($columns[$x] === '#') {
            $potentials[] = [
                'x' => $x,
                'y' => $y,
                'countVisible' => 0,
            ];
        }
    }
}

for ($i = 0, $c = count($potentials); $i < $c; $i++) {
    $candidate = $potentials[$i];
    $ratios = [];
    foreach ($potentials as $test) {
        $ratios[] = getRatio($candidate['x'], $candidate['y'], $test['x'], $test['y']);
    }
    $ratios = array_unique($ratios);
    $potentials[$i]['countVisible'] = count($ratios);
}

$result = null;
foreach ($potentials as $candidate) {
    if ($result === null || $candidate['countVisible'] > $result['countVisible']) {
        $result = $candidate;
    }
}

var_dump($result);

function getRatio($startX, $startY, $endX, $endY) {
    return atan2($endY - $startY, $endX - $startX);
}