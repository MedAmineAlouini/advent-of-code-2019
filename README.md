# Advent of Code 2019

This is the code that Chris Forrence will be publishing to show his code for Advent of Code 2019!

## Languages

The languages that I have used are as follows (generally, PHP will be the first solution attempted)

- PHP (1,2)
- Rust (1)