<?php

$input = file_get_contents(__DIR__ . '/input.txt');
$wires = explode("\n", $input);

$mapping = [
    'U' => [0, 1],
    'D' => [0, -1],
    'L' => [-1, 0],
    'R' => [1, 0],
];


$endpoints = [];

foreach ($wires as $wire) {
    $position = [0, 0];
    $distance = 0;
    $wire_points = [];
    $directions = explode(',', $wire);
    foreach ($directions as $direction) {
        $facing = substr($direction, 0, 1);
        $magnitude = intval(substr($direction, 1));

        for ($i = 1; $i <= $magnitude; $i++) {
            $position[0] += $mapping[$facing][0];
            $position[1] += $mapping[$facing][1];

            $wire_points["{$position[0]},{$position[1]}"] = ++$distance;
        }
    }
    $endpoints[] = $wire_points;
}
$intersections = array_intersect_key($endpoints[0], $endpoints[1]);
$distances = [];
foreach ($intersections as $intersection => $value) {
    $distances[] = $endpoints[0][$intersection]+$endpoints[1][$intersection];
}

echo min($distances);
