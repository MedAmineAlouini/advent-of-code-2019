<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$input = explode("\n", $input);
function getFuel($mass) {
    $estimated = floor($mass / 3) - 2;

    if ($estimated <= 0) {
        return 0;
    }

    return $estimated + getFuel($estimated);
}

$output = 0;

foreach ($input as $el) {
    $output += getFuel($el);
}

var_dump($output);