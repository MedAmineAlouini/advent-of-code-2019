<?php

$min = 402328;
$max = 864247;

$count = 0;
for ($i = $min; $i <= $max; $i++) {
    $digits = str_split($i);
    if (areTwoAdjecentDigitsEqual($digits) && areDigitsIncreasingOrEqual($digits)) {
        $count++;
    }
}

echo $count;

function areTwoAdjecentDigitsEqual($digits) {
    return $digits[0] === $digits[1]
        || $digits[1] === $digits[2]
        || $digits[2] === $digits[3]
        || $digits[3] === $digits[4]
        || $digits[4] === $digits[5];
}
function areDigitsIncreasingOrEqual($digits) {
    return $digits[0] <= $digits[1]
        && $digits[1] <= $digits[2]
        && $digits[2] <= $digits[3]
        && $digits[3] <= $digits[4]
        && $digits[4] <= $digits[5];
}