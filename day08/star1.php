<?php

$input = file_get_contents('input.txt');

var_dump(strlen($input));

$raw = str_split($input, 25 * 6);
$layers = [];
foreach ($raw as $layer) {
    $layers[] = [
        'raw' => $layer,
        '0es' => strlen(str_replace(['1', '2'], '', $layer)),
        '1es' => strlen(str_replace(['0', '2'], '', $layer)),
        '2es' => strlen(str_replace(['0', '1'], '', $layer)),
    ];
}

var_dump($layers);

$result = ['0es' => PHP_INT_MAX, 'checksum' => 0];
foreach ($layers as $layer) {
    if ($layer['0es'] < $result['0es']) {
        $result['0es'] = $layer['0es'];
        $result['checksum'] = $layer['1es'] * $layer['2es'];
    }
}

var_dump($result);
