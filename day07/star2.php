<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$codes = explode(',', $input);

$inputSignal = 0;

$tests = [];
for ($a = 5; $a < 10; $a++) {
    for ($b = 5; $b < 10; $b++) {
        if (in_array($b, [$a])) { continue; }
        for ($c = 5; $c < 10; $c++) {
            if (in_array($c, [$a, $b])) { continue; }
            for ($d = 5; $d < 10; $d++) {
                if (in_array($d, [$a, $b, $c])) { continue; }
                for ($e = 5; $e < 10; $e++) {
                    if (in_array($e, [$a, $b, $c, $d])) { continue; }
                    $tests[] = "{$a}{$b}{$c}{$d}{$e}";
                }
            }
        }
    }
}

$max = 0;

foreach ($tests as $test) {
    echo 'Testing ' . $test . '...';
    $phases = str_split($test);

    $computers = [
        new Intcode($codes, $phases[0]),
        new Intcode($codes, $phases[1]),
        new Intcode($codes, $phases[2]),
        new Intcode($codes, $phases[3]),
        new Intcode($codes, $phases[4]),
    ];

    $idx = 0;
    $inputSignal = 0;
    while (true) {
        $computer = $computers[$idx % 5];
        $computer->process($inputSignal);
        $computers[$idx % 5] = $computer;
        $inputSignal = $computer->getOutput();
        if ($idx === sizeof($computers) - 1 && $computers[$idx]->isHalted()) {
            echo $computers[sizeof($computers) - 1]->getOutput() . PHP_EOL;
            $result = $computers[sizeof($computers) - 1]->getOutput() . PHP_EOL;
            break;
        }
        $idx = ($idx + 1) % 5;
    }
    $max = max($max, $result);
}

echo ($max);

class Intcode {
    protected $codes;
    protected $phaseSetting;
    protected $i;
    protected $isStopped;
    protected $output;
    protected $hasProvidedPhase;

    public function __construct($codes, $phaseSetting)
    {
        $this->codes = $codes;
        $this->phaseSetting = $phaseSetting;
        $this->i = 0;
        $this->isStopped = false;
        $this->output = null;
        $this->hasProvidedPhase = false;
    }

    private function getParameterValue($mode, $codes, $place) {
        if ($mode == 1) {
            // Value
            return $codes[$place];
        }
        return $codes[$codes[$place]];
    }

    private function getMode($parameter, $opcode) {
        $opcode = $opcode / 100;
        if ($parameter == 1 && $opcode % 10 > 0) {
            return 1;
        } else if ($parameter == 2 && ($opcode / 10) % 10 > 0) {
            return 1;
        }
        return 0;
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function isHalted() {
        return $this->isStopped === true;
    }

    public function process($inputSignal) {
        if ($this->isHalted()) {
            return;
        }
        // var_dump(implode(',', $this->codes));
        // var_dump('Starting $i: ' . $this->i);
        for ($c = count($this->codes); $this->i < $c; $this->i+=0) {
            $opcode = $this->codes[$this->i] % 100;
            // var_dump('..Current $i: ' . $this->i);
            switch ($opcode) {
                case '1':
                    $result = $this->getParameterValue($this->getMode(1, $this->codes[$this->i]), $this->codes, $this->i + 1)
                            + $this->getParameterValue($this->getMode(2, $this->codes[$this->i]), $this->codes, $this->i + 2);

                    $this->codes[$this->codes[$this->i + 3]] = $result;
                    $this->i += 4;

                    // var_dump(implode(',', $this->codes) . ' (1)');
                    break;
                case '2':
                    $result = $this->getParameterValue($this->getMode(1, $this->codes[$this->i]), $this->codes, $this->i + 1)
                            * $this->getParameterValue($this->getMode(2, $this->codes[$this->i]), $this->codes, $this->i + 2);

                    $this->codes[$this->codes[$this->i + 3]] = $result;
                    $this->i += 4;
                    // var_dump(implode(',', $this->codes) . ' (2)');
                    break;
                case '3':
                    $input = $this->hasProvidedPhase ? $inputSignal : $this->phaseSetting;
                    $this->hasProvidedPhase = true;
                    $this->codes[$this->codes[$this->i + 1]] = $input;
                    $this->i += 2;
                    // var_dump('input: ' . $input);
                    // var_dump(implode(',', $this->codes) . ' (3)');
                    break;
                case '4':
                    $this->output = $this->codes[$this->codes[$this->i + 1]];
                    $this->i += 2;
                    $this->isStopped = false;
                    // var_dump('output: ' . $this->output);
                    // var_dump(implode(',', $this->codes) . ' (4)');
                    break 2;
                case '5':
                    if ($this->getParameterValue($this->getMode(1, $this->codes[$this->i]), $this->codes, $this->i + 1) != 0) {
                        $this->i = $this->getParameterValue($this->getMode(2, $this->codes[$this->i]), $this->codes, $this->i + 2);
                        break;
                    }
                    $this->i += 3;
                    // var_dump(implode(',', $this->codes) . ' (5)');
                    break;
                case '6':
                    if ($this->getParameterValue($this->getMode(1, $this->codes[$this->i]), $this->codes, $this->i + 1) == 0) {
                        $this->i = $this->getParameterValue($this->getMode(2, $this->codes[$this->i]), $this->codes, $this->i + 2);
                        break;
                    }
                    $this->i += 3;
                    // var_dump(implode(',', $this->codes) . ' (6)');
                    break;
                case '7':
                    $this->codes[$this->codes[$this->i + 3]] = (
                        $this->getParameterValue($this->getMode(1, $this->codes[$this->i]), $this->codes, $this->i + 1) < $this->getParameterValue($this->getMode(2, $this->codes[$this->i]), $this->codes, $this->i + 2)
                    ) ? 1 : 0;
                    $this->i += 4;
                    // var_dump(implode(',', $this->codes) . ' (7)');
                    break;
                case '8':
                    $this->codes[$this->codes[$this->i + 3]] = (
                        $this->getParameterValue($this->getMode(1, $this->codes[$this->i]), $this->codes, $this->i + 1) == $this->getParameterValue($this->getMode(2, $this->codes[$this->i]), $this->codes, $this->i + 2)
                    ) ? 1 : 0;
                    $this->i += 4;
                    // var_dump(implode(',', $this->codes) . ' (8)');
                    break;
                case '99':
                    $this->isStopped = true;
                    // var_dump('halting');
                    break 2;
                default:
                    echo 'wat. ' . $opcode . PHP_EOL;
                    exit(1);
            }
        }
        // var_dump(implode(',', $this->codes));

    }
}