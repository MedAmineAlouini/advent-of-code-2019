<?php

$input = file_get_contents(__DIR__ . '/input.txt');

$codes = explode(',', $input);

$intcode = new Intcode($codes);

$inputSignal = 0;

$tests = [];
for ($a = 0; $a < 5; $a++) {
    for ($b = 0; $b < 5; $b++) {
        if (in_array($b, [$a])) { continue; }
        for ($c = 0; $c < 5; $c++) {
            if (in_array($c, [$a, $b])) { continue; }
            for ($d = 0; $d < 5; $d++) {
                if (in_array($d, [$a, $b, $c])) { continue; }
                for ($e = 0; $e < 5; $e++) {
                    if (in_array($e, [$a, $b, $c, $d])) { continue; }
                    $tests[] = "{$a}{$b}{$c}{$d}{$e}";
                }
            }
        }
    }
}

$max = 0;

foreach ($tests as $test) {
    echo 'Testing ' . $test . '...';
    $inputSignal = 0;
    foreach (str_split($test) as $phaseSetting) {
        $inputSignal = $intcode->process($phaseSetting, $inputSignal);
    }
    echo $inputSignal . PHP_EOL;
    $max = max($inputSignal, $max);
}

var_dump($max);

class Intcode {
    protected $codes;

    public function __construct($codes)
    {
        $this->codes = $codes;
    }

    private function getParameterValue($mode, $codes, $place) {
        if ($mode == 1) {
            // Value
            return $codes[$place];
        }
        return $codes[$codes[$place]];
    }

    private function getMode($parameter, $opcode) {
        $opcode = $opcode / 100;
        if ($parameter == 1 && $opcode % 10 > 0) {
            return 1;
        } else if ($parameter == 2 && ($opcode / 10) % 10 > 0) {
            return 1;
        }
        return 0;
    }

    public function process($phaseSetting, $inputSignal) {
        echo "process({$phaseSetting}, {$inputSignal})" . PHP_EOL;
        $codes = [] + $this->codes;
        // var_dump(implode(',', $codes));
        $hasProvidedPhase = false;

        for ($i = 0, $c = count($codes); $i < $c; $i+=0) {
            $opcode = $codes[$i] % 100;
            switch ($opcode) {
                case '1':
                    $result = $this->getParameterValue($this->getMode(1, $codes[$i]), $codes, $i + 1)
                            + $this->getParameterValue($this->getMode(2, $codes[$i]), $codes, $i + 2);

                    $codes[$codes[$i + 3]] = $result;
                    $i += 4;

                    // var_dump(implode(',', $codes) . ' (1)');
                    break;
                case '2':
                    $result = $this->getParameterValue($this->getMode(1, $codes[$i]), $codes, $i + 1)
                            * $this->getParameterValue($this->getMode(2, $codes[$i]), $codes, $i + 2);

                    $codes[$codes[$i + 3]] = $result;
                    $i += 4;
                    // var_dump(implode(',', $codes) . ' (2)');
                    break;
                case '3':
                    $input = $hasProvidedPhase ? $inputSignal : $phaseSetting;
                    $hasProvidedPhase = true;
                    $codes[$codes[$i + 1]] = $input;
                    $i += 2;
                    // var_dump(implode(',', $codes) . ' (3)');
                    break;
                case '4':
                    $output = $codes[$codes[$i + 1]];
                    $i += 2;
                    // var_dump(implode(',', $codes) . ' (4)');
                    break;
                case '5':
                    if ($this->getParameterValue($this->getMode(1, $codes[$i]), $codes, $i + 1) != 0) {
                        $i = $this->getParameterValue($this->getMode(2, $codes[$i]), $codes, $i + 2);
                        break;
                    }
                    $i += 3;
                    // var_dump(implode(',', $codes) . ' (5)');
                    break;
                case '6':
                    if ($this->getParameterValue($this->getMode(1, $codes[$i]), $codes, $i + 1) == 0) {
                        $i = $this->getParameterValue($this->getMode(2, $codes[$i]), $codes, $i + 2);
                        break;
                    }
                    $i += 3;
                    // var_dump(implode(',', $codes) . ' (6)');
                    break;
                case '7':
                    $codes[$codes[$i + 3]] = (
                        $this->getParameterValue($this->getMode(1, $codes[$i]), $codes, $i + 1) < $this->getParameterValue($this->getMode(2, $codes[$i]), $codes, $i + 2)
                    ) ? 1 : 0;
                    $i += 4;
                    // var_dump(implode(',', $codes) . ' (7)');
                    break;
                case '8':
                    $codes[$codes[$i + 3]] = (
                        $this->getParameterValue($this->getMode(1, $codes[$i]), $codes, $i + 1) == $this->getParameterValue($this->getMode(2, $codes[$i]), $codes, $i + 2)
                    ) ? 1 : 0;
                    $i += 4;
                    // var_dump(implode(',', $codes) . ' (8)');
                    break;
                case '99':
                    break 2;
                default:
                    echo 'wat. ' . $opcode . PHP_EOL;
                    exit(1);
            }
        }
        // var_dump(implode(',', $codes));

        return $output;
    }


}