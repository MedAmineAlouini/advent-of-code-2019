<?php

namespace Forrence\AdventOfCode;

class Intcode
{
    private $code = [];
    private $input = [];
    private $output = [];
    private $iterator = 0;
    private $relative = 0;
    private $debugMode = false;
    private $halted = false;

    public $opcodes = [
        '01' => ['id' => 1, 'description' => 'add',             'advance' => 4],
        '02' => ['id' => 2, 'description' => 'multiply',        'advance' => 4],
        '03' => ['id' => 3, 'description' => 'input',           'advance' => 2],
        '04' => ['id' => 4, 'description' => 'output',          'advance' => 2],
        '05' => ['id' => 5, 'description' => 'goto-if-nonzero', 'advance' => 3],
        '06' => ['id' => 6, 'description' => 'goto-if-zero',    'advance' => 3],
        '07' => ['id' => 7, 'description' => 'bool-if-lt',      'advance' => 4],
        '08' => ['id' => 8, 'description' => 'bool-if-eq',      'advance' => 4],
        '09' => ['id' => 9, 'description' => 'adjust',          'advance' => 2],
        '99' => ['id' => 99,'description' => 'halt',            'advance' => 0],
    ];

    public function __construct() {}

    public function setCode($code) {
        $this->code = $code;
        return $this;
    }

    public function getInput() {
        return array_pop($this->input);
    }

    public function setInput($input) {
        $this->input = $input;
        return $this;
    }

    public function addInput($input) {
        $this->input[] = $input;
        return $this;
    }

    public function isDebugMode() {
        return $this->debugMode;
    }

    public function setDebugMode($debugMode) {
        $this->debugMode = $debugMode;
        return $this;
    }

    public function isHalted() {
        return $this->halted;
    }

    public function setHalted($halted) {
        $this->halted = $halted;
        return $this;
    }

    public function getOutput() {
        return $this->output;
    }

    public function setOutput($output) {
        $this->output = $output;
        return $this;
    }

    public function addOutput($output) {
        $this->output[] = $output;
        return $this;
    }

    public function opcode($code) {
        $opcode = str_pad($code % 100, 2, '0', STR_PAD_LEFT);
        if (!isset($this->opcodes[$opcode])) {
            throw new \InvalidArgumentException('Opcode ' . $opcode . ' is unknown');
        }

        // 101 => [001] => arg 1 = 1, arg 2 = 0, arg 3 = 0 => [1, 0, 0]
        $modes = floor($code / 100); // 101 -> 1
        $modes = str_pad($modes, 3, '0', STR_PAD_LEFT) . '0'; // 1 -> 0010
        $modes = str_split($modes);
        $modes = array_reverse($modes);

        return [
            $this->opcodes[$opcode],
            $modes
        ];
    }

    private function value($mode, $idx) {
        $this->debug('idx=' . $idx . ', relative=' . $this->relative . ', ' . ($this->code[$this->code[$idx]] ?? 'null') . '/' . ($this->code[$idx] ?? 'null') . '/' . ($this->code[$this->code[$idx] + $this->relative] ?? 'null'));
        switch ($mode) {
            case 2: // Relative Mode
                if (!isset($this->code[$idx])) {
                    return 0;
                }
                return $this->code[$this->code[$idx] + $this->relative] ?? 0;
            case 1: // Immediate Mode
                if (!isset($this->code[$idx])) {
                    return 0;
                }
                return $this->code[$idx] ?? 0;
            case 0: // Position Mode
                if (!isset($this->code[$idx])) {
                    return 0;
                }
                return $this->code[$this->code[$idx]] ?? 0;
            default:
                throw new \InvalidArgumentException('Unknown mode ' . $mode);
        }
    }

    private function write($mode, $idx, $value) {
        switch ($mode) {
            case 2: // Relative Mode
                if (!isset($this->code[$idx])) {
                    $this->code[$idx] = 0;
                }
                $this->code[$this->code[$idx] + $this->relative] = $value;
                break;
            case 0: // Position Mode
                if (!isset($this->code[$idx])) {
                    $this->code[$idx] = 0;
                }
                $this->code[$this->code[$idx]] = $value;
                break;
            default:
                throw new \InvalidArgumentException('Unknown write mode ' . $mode);
        }
    }

    private function debug($message) {
        if ($this->debugMode) {
            echo '[' . ($this->iterator+1) . ']: ' . $message . PHP_EOL;
        }
    }

    public function process() {
        if ($this->isHalted()) {
            return $this->getOutput() ?: $this->code[0];
        }

        for ($c = count($this->code); $this->iterator < $c;) {
            $absAdvance = false;
            list($opcode, $modes) = $this->opcode($this->code[$this->iterator]);

            if (!$opcode['advance']) {
                $this->debug('Halted');
                break;
            }

            switch($opcode['id']) {
                case 1: // Add
                    $this->debug('add ' . $this->value($modes[1], $this->iterator + 1) . ' (mode ' . $modes[1] . ') and ' . $this->value($modes[2], $this->iterator + 2) . ' (mode ' . $modes[2] . ')');
                    $result = $this->value($modes[1], $this->iterator + 1)
                                + $this->value($modes[2], $this->iterator + 2);
                    $this->write($modes[3], $this->iterator + 3, $result);
                    break;
                case 2: // Multiply
                    $this->debug('multiply ' . $this->value($modes[1], $this->iterator + 1) . ' (mode ' . $modes[1] . ') and ' . $this->value($modes[2], $this->iterator + 2) . ' (mode ' . $modes[2] . ')');
                    $result = $this->value($modes[1], $this->iterator + 1)
                                * $this->value($modes[2], $this->iterator + 2);
                    $this->write($modes[3], $this->iterator + 3, $result);
                    break;
                case 3: // Input
                    $this->write($modes[1], $this->iterator + 1, $this->getInput());
                    break;
                case 4: // Output
                    $this->debug('adding ' . $this->value($modes[1], $this->iterator + 1) . ' to output (mode ' . $modes[1] . ') ');
                    $this->addOutput($this->value($modes[1], $this->iterator + 1));
                    break;
                case 5: // goto-if-nonzero
                    $this->debug('go to ' . $this->value($modes[2], $this->iterator + 2) . ' (mode ' . $modes[2] . ') ' . ' if ' . $this->value($modes[1], $this->iterator + 1) . ' (mode ' . $modes[1] . ') is nonzero');
                    if ($this->value($modes[1], $this->iterator + 1) != 0) {
                        $absAdvance = $this->value($modes[2], $this->iterator + 2);
                    }
                    break;
                case 6: // goto-if-zero
                    $this->debug('go to ' . $this->value($modes[2], $this->iterator + 2) . ' (mode ' . $modes[2] . ' if ' . $this->value($modes[1], $this->iterator + 1) . ' (mode ' . $modes[1] . ') is zero');
                    if ($this->value($modes[1], $this->iterator + 1) == 0) {
                        $absAdvance = $this->value($modes[2], $this->iterator + 2);
                    }
                    break;
                case 7: // bool-if-lt
                    $this->debug('bool-if-lt ' . $this->value($modes[1], $this->iterator + 1) . ' (mode ' . $modes[1] . ') vs ' . $this->value($modes[2], $this->iterator + 2) . ' (mode ' . $modes[2] . ')');
                    $this->write(
                        $modes[3],
                        $this->iterator + 3,
                        $this->value($modes[1], $this->iterator + 1) < $this->value($modes[2], $this->iterator + 2) ? 1 : 0
                    );
                    break;
                case 8: // bool-if-eq
                    $this->debug('bool-if-eq ' . $this->value($modes[1], $this->iterator + 1) . ' (mode ' . $modes[1] . ') vs ' . $this->value($modes[2], $this->iterator + 2) . ' (mode ' . $modes[2] . ')');
                    $this->write(
                        $modes[3],
                        $this->iterator + 3,
                        $this->value($modes[1], $this->iterator + 1) == $this->value($modes[2], $this->iterator + 2) ? 1 : 0
                    );
                    break;
                case 9: // adjust-relative
                    $this->debug('Adjusting relative from ' . $this->relative . ' by ' . $this->value($modes[1], $this->iterator + 1) . ' (mode' . $modes[1] . ')');
                    $this->relative += $this->value($modes[1], $this->iterator + 1);
                    break;
            }
            if ($absAdvance !== false) {
                $this->iterator = $absAdvance;
            } else {
                $this->iterator += $opcode['advance'];
            }
        }
        if (empty($this->getOutput())) {
            return $this->code[0];
        }
        return $this->getOutput();
    }
}