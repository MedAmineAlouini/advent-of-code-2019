<?php

use PHPUnit\Framework\TestCase;

class Day09Test extends TestCase
{
    public function setUp(): void
    {
        $this->intcode = new Forrence\AdventOfCode\Intcode;
        $this->intcode->setDebugMode(true);
    }

    public function testOpcodesWork()
    {
        $opcodes = [
            '1' => [$this->intcode->opcodes['01'], ['0', '0', '0', '0']],
            '101' => [$this->intcode->opcodes['01'], ['0', '1', '0', '0']],
            '2101' => [$this->intcode->opcodes['01'], ['0', '1', '2', '0']],
            '109' => [$this->intcode->opcodes['09'], ['0', '1', '0', '0']],
        ];
        foreach ($opcodes as $test => $expected) {
            $this->assertEquals($expected, $this->intcode->opcode($test));
        }
    }

    public function testDay09Sample1Works()
    {
        // Arrange
        $input = '109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99';
        $code = explode(',', $input);

        $this->intcode->setCode($code);

        // Act
        $output = $this->intcode->process();

        // Assert
        $this->assertEquals(['109','1','204','-1','1001','100','1','100','1008','100','16','101','1006','101','0','99'], $output);
    }

    public function testDay09Sample2Works()
    {
        // Arrange
        $input = '1102,34915192,34915192,7,4,7,99,0';
        $code = explode(',', $input);

        $this->intcode->setCode($code);

        // Act
        $output = $this->intcode->process();

        // Assert
        $this->assertEquals(16, strlen($output[0]));
    }

    public function testDay09Sample3Works()
    {
        // Arrange
        $input = '104,1125899906842624,99';
        $code = explode(',', $input);

        $this->intcode->setCode($code);

        // Act
        $output = $this->intcode->process();

        // Assert
        $this->assertEquals([1125899906842624], $output);
    }
}