<?php
ini_set('memory_limit', '8192M');

require 'vendor/autoload.php';

$input = explode("\n", file_get_contents('input.txt'));

/*
<x=1, y=2, z=-9>
<x=-1, y=-9, z=-4>
<x=17, y=6, z=8>
<x=12, y=4, z=2>
 */
$moons = [];
$labels = ['A', 'B', 'C', 'D'];
$idx = 0;
foreach ($input as $line) {
    $txt = str_replace(['<', '>', 'x', 'y', 'z', ' ', '='], '', $line);
    list($x, $y, $z) = explode(',', $txt);
    $moons[] = [
        'label' => $labels[$idx++],
        'position' => [
            'x' => $x,
            'y' => $y,
            'z' => $z,
        ],
        'velocity' => [
            'x' => 0,
            'y' => 0,
            'z' => 0,
        ],
    ];
}

$seen = ['x' => [], 'y' => [], 'z' => []];
$repeats = [];
$dimensions = ['x', 'y', 'z'];
$i = 0;
while (count($repeats) < 3) {
    $moons = updateVelocitys($moons);
    $moons = updatePositions($moons);

    foreach ($dimensions as $dimension) {
        if (isset($repeats[$dimension])) {
            continue;
        }
        $signature = getDimensionSignature($moons, $dimension);
        if (isset($seen[$dimension][$signature])) {
            $repeats[$dimension] = $i;
        } else {
            $seen[$dimension][$signature] = true;
        }
    }

    $i++;
}

var_dump(lcm(lcm($repeats['x'], $repeats['y']), $repeats['z']));

function getDimensionSignature($moons, $dimension) {
    $response = [];
    foreach ($moons as $moon) {
        $response[] = $moon['position'][$dimension];
        $response[] = $moon['velocity'][$dimension];
    }
    return implode(',', $response);
}
function updateVelocitys($moons) {
    for ($i = 0, $c = count($moons); $i < $c; $i++) {
        for ($j = 0, $d = count($moons); $j < $d; $j++) {
            if ($i === $j) { continue; }
            $moons[$i]['velocity']['x'] += -1 * ($moons[$i]['position']['x'] <=> $moons[$j]['position']['x']);
            $moons[$i]['velocity']['y'] += -1 * ($moons[$i]['position']['y'] <=> $moons[$j]['position']['y']);
            $moons[$i]['velocity']['z'] += -1 * ($moons[$i]['position']['z'] <=> $moons[$j]['position']['z']);
        }
    }
    return $moons;
}

function updatePositions($moons) {
    for ($i = 0, $c = count($moons); $i < $c; $i++) {
        $moon = $moons[$i];
        $moon['position']['x'] += $moon['velocity']['x'];
        $moon['position']['y'] += $moon['velocity']['y'];
        $moon['position']['z'] += $moon['velocity']['z'];
        $moons[$i] = $moon;
    }
    return $moons;
}

function getPotentialEnergy($moon) {
    return abs($moon['position']['x'])
        + abs($moon['position']['y'])
        + abs($moon['position']['z']);
}
function getKineticEnergy($moon) {
    return abs($moon['velocity']['x'])
        + abs($moon['velocity']['y'])
        + abs($moon['velocity']['z']);

}
function getTotalEnergy($moons) {
    $result = 0;
    foreach ($moons as $moon) {
        $result += getPotentialEnergy($moon) * getKineticEnergy($moon);
    }
    return $result;
}

/* https://www.php.net/manual/en/ref.math.php#70969 */
function lcm_arr($items){
    //Input: An Array of numbers
    //Output: The LCM of the numbers
    while(2 <= count($items)){
        array_push($items, lcm(array_shift($items), array_shift($items)));
    }
    return reset($items);
}


/* https://www.php.net/manual/en/ref.math.php#70969 */
function gcd($n, $m) {
   $n=abs($n); $m=abs($m);
   if ($n==0 and $m==0)
       return 1; //avoid infinite recursion
   if ($n==$m and $n>=1)
       return $n;
   return $m<$n?gcd($n-$m,$n):gcd($n,$m-$n);
}

/* https://www.php.net/manual/en/ref.math.php#70969 */
function lcm($n, $m) {
   return $m * ($n/gcd($n,$m));
}